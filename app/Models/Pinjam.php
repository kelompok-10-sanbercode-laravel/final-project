<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pinjam extends Model
{
    use HasFactory;

    protected $table = 'peminjaman';

    protected $fillable = ['petugas_id', 'anggota_id', 'buku_id', 'tgl_peminjaman', 'tgl_kembali'];
}
