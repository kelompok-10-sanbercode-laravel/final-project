<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\kategori_buku;
use App\Models\Buku;

class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $buku= Buku::get();
      $kategori_buku = kategori_buku::get();
      return view('buku.index', ['buku' => $buku, 'kategori_buku' => $kategori_buku]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $buku = Buku::get();
        $kategori_buku = kategori_buku::get();
        return view('buku.create', ['buku' => $buku, 'kategori_buku' => $kategori_buku]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'pengarang' => 'required',
            'judul' => 'required',
            'penerbit' => 'required',
            'year' => 'required',
            'kategori_buku_id' => 'required'
        ] );


        $buku = new Buku;
        $buku->pengarang = $request->pengarang;
        $buku->judul = $request->judul;
        $buku->penerbit = $request->penerbit;
        $buku->year= $request->year;
        $buku->kategori_buku_id= $request->kategori_buku_id;

        $buku -> save();
        return redirect('/buku');
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $buku= Buku::find($id);
        $kategori_buku = kategori_buku::get();

        return view('buku.update', ['buku'=>$buku, 'kategori_buku' => $kategori_buku]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'pengarang' => 'required',
            'judul' => 'required',
            'penerbit' => 'required',
            'year' => 'required',
            'kategori_buku_id' => 'required'
        ] );

        $buku = Buku::find($id);

        $buku->pengarang = $request['pengarang'];
        $buku->judul = $request['judul'];
        $buku->penerbit = $request['penerbit'];
        $buku->year = $request['year'];
        $buku->kategori_buku_id = $request['kategori_buku_id'];
        $buku->save();

        return redirect('/buku');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $buku = Buku::find($id);
        $buku->delete();
        return redirect('/buku');
    }
}
