<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pinjam;
use App\Models\kategori_buku;

class PeminjamanController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $pinjam= Pinjam::get();
      return view('peminjaman.index', ['peminjaman' => $pinjam]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori_buku = kategori_buku::get();
        return view('peminjaman.create', ['kategori_buku' => $kategori_buku]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'petugas_id' => 'required',
            'anggota_id' => 'required',
            'buku_id' => 'required',
            'tgl_peminjaman' => 'required',
            'tgl_kembali' => 'required'
        ] );


        $pinjam = new Pinjam();
        $pinjam->petugas_id = $request->petugas_id;
        $pinjam->anggota_id = $request->anggota_id;
        $pinjam->buku_id = $request->buku_id;
        $pinjam->tgl_peminjaman= $request->tgl_peminjaman;
        $pinjam->tgl_kembali= $request->tgl_kembali;

        $pinjam -> save();
        return redirect('/peminjaman');
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pinjam= Pinjam::find($id);
        $kategori_buku = kategori_buku::get();

        return view('buku.update', ['buku'=>$pinjam, 'kategori_buku' => $kategori_buku]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'petugas_id' => 'required',
            'anggota_id' => 'required',
            'buku_id' => 'required',
            'tgl_peminjaman' => 'required',
            'tgl_kembali' => 'required'
        ] );

        $pinjam = Pinjam::find($id);
        
        $pinjam->petugas_id = $request['petugas_id'];
        $pinjam->anggota_id = $request['anggota_id'];
        $pinjam->buku_id = $request['buku_id'];
        $pinjam->tgl_peminjaman= $request['tgl_peminjaman'];
        $pinjam->tgl_kembali= $request['tgl_kembali'];

        $pinjam->save();

        return redirect('/peminjaman');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pinjam = Pinjam::find($id);
        $pinjam->delete();
        return redirect('/peminjaman');
    }
}
