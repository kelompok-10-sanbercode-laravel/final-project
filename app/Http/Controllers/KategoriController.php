<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\kategori_buku;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategori_buku = kategori_buku::get();
        return view('kategori.index', ['kategori_buku' => $kategori_buku]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori_buku = kategori_buku::get();
        return view('kategori.create', ['kategori_buku' => $kategori_buku]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kategori' => 'required',
        ] );


        $kategori_buku = new kategori_buku();
        $kategori_buku->kategori = $request->kategori;

        $kategori_buku -> save();
        return redirect('/kategori_buku');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori_buku = kategori_buku::find($id);

        return view('kategori.update', ['kategori_buku' => $kategori_buku]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kategori' => 'required',
        ] );

        $kategori_buku = kategori_buku::find($id);

        $kategori_buku->kategori = $request['kategori'];
        $kategori_buku->save();

        return redirect('/kategori_buku');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kategori_buku = kategori_buku::find($id);
        $kategori_buku->delete();
        return redirect('/kategori_buku');
    }
}
