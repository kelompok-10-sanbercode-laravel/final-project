<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\profile;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function index(){
        $idpetugas = Auth::id();

        $detailProfile = Profile::where('petugas_id', $idpetugas)->first();

        return view('profile.index', ['detailProfile' => $detailProfile]);
    }
    
    public function update(Request $request, $id){
        $request->validate([
            'alamat' => 'required',
            'jk' => 'required',
            'hp' => 'required',
        ]);

        $profile = profile::find($id);

        $profile->alamat = $request->alamat;
        $profile->jk = $request->jk;
        $profile->hp = $request->hp;

        $profile->save();

        return redirect('/profile');
    }
}
