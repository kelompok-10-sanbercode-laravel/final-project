<?php

namespace App\Http\Controllers;

use App\Models\anggota;
use Illuminate\Http\Request;

class AnggotaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $anggota= anggota::get();
        return view('anggota.index', ['anggota' => $anggota]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $anggota = anggota::get();
        return view('anggota.create', ['anggota' => $anggota]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'alamat' => 'required',
            'jk' => 'required',
            'hp' => 'required'
        ] );


        $anggota = new anggota();
        $anggota->nama = $request->nama;
        $anggota->alamat = $request->alamat;
        $anggota->jk = $request->jk;
        $anggota->hp = $request->hp;

        $anggota -> save();
        return redirect('/anggota');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $anggota= anggota::find($id);

        return view('anggota.update', ['anggota' => $anggota]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'alamat' => 'required',
            'jk' => 'required',
            'hp' => 'required'
        ] );

        $anggota = anggota::find($id);

        $anggota->nama = $request['nama'];
        $anggota->alamat = $request['alamat'];
        $anggota->jk = $request['jk'];
        $anggota->hp = $request['hp'];
        $anggota->save();

        return redirect('/anggota');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $anggota = anggota::find($id);
        $anggota->delete();
        return redirect('/anggota');
    }
}
