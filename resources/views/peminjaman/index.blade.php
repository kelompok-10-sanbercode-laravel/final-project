@extends('layouts.master')

@section('content')
<div class="content-wrapper">
    <div class="page-header">
      <h3 class="page-title"> List Data Peminjam </h3>
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Peminjam</a></li>
          <li class="breadcrumb-item active" aria-current="page">List Peminjam</li>
        </ol>
      </nav>
    </div>
    <div class="row">
      <a href="/peminjaman/create" class="btn btn-primary mb-3 mx-3">
        Tambah
    </a>
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <div class="table">
              <table class="table">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Id Petugas</th>
                    <th>Id Anggota</th>
                    <th>Id Buku</th>
                    <th>Tanggal Peminjaman</th>
                    <th>Tanggal Pengembalian</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @forelse ($peminjaman as $key => $value)
                  <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$value->petugas_id}}</td>
                    <td>{{$value->anggota_id}}</td>
                    <td>{{$value->buku_id}}</td>
                    <td>{{$value->tgl_peminjaman}}</td>
                    <td>{{$value->tgl_kembali}}</td>
                    <td>
                      <form action="/peminjaman/{{$value->id}}" method="POST" enctype="multipart/form-data">
                          @csrf
                          @method('DELETE')
                          <a href="/buku/{{$value->id}}/edit" class="btn btn-warning btn-sm m-1">Edit</a>
                          <input type="submit" value="Delete" class="btn btn-danger btn-sm m-1">
                      </form>
                    </td>
                  </tr>
                  @empty
                  <tr>
                    <td>Tidak Ada Data</td>
                  </tr>
                  @endforelse
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>
@endsection