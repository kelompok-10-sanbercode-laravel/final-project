@extends('layouts.master')

@section('content')
    <div class="content-wrapper">
      <div class="page-header">
        <h3 class="page-title"> Tambah Data Peminjam </h3>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="">Peminjaman</a></li>
            <li class="breadcrumb-item active" aria-current="page">Tambah Peminjam</li>
          </ol>
        </nav>
      </div>
      <div class="row">
        <div class="col-12 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
              <form class="forms-sample" action="/buku/{{$pinjam->id}}" method="POST"  enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="form-group">
                  <label for="exampleInputName1">Petugas</label>
                  <input name="petugas_id" type="text" class="form-control" value="{{$pinjam->petugas_id}}">
                </div>
                @error('petugas_id')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label for="exampleTextarea1">Peminjam</label>
                    <input name="anggota_id" type="text" class="form-control" value="{{$pinjam->anggota_id}}">
                </div>
                @error('anggota_id')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                  <label for="exampleInputCity1">Buku</label>
                  <input name="buku_id" type="text" class="form-control" value="{{$pinjam->buku_id}}">
                </div>
                @error('buku_id')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                  <label for="exampleInputCity1">Tanggal Peminjaman</label>
                  <input name="tgl_peminjaman" type="text" class="form-control" value="{{$pinjam->tgl_peminjaman}}">
                </div>
                @error('tgl_peminjaman')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                  <label for="exampleInputCity1">Tanggal Pengembalian</label>
                  <input name="tgl_kembali" type="text" class="form-control" value="{{$pinjam->tgl_kembali}}">
                </div>
                @error('tgl_kembali')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <button type="submit" class="btn btn-primary mr-2">Submit</button>
                <button class="btn btn-light">Cancel</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection