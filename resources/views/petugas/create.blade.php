@extends('layouts.master')

@section('content')
    <div class="content-wrapper">
      <div class="page-header">
        <h3 class="page-title"> Tambah Data Petugas </h3>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="">Petugas</a></li>
            <li class="breadcrumb-item active" aria-current="page">Tambah Petugas</li>
          </ol>
        </nav>
      </div>
      <div class="row">
        <div class="col-12 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
              <form class="forms-sample" action="/petugas" method="POST"  enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                  <label for="exampleInputName1">Nama</label>
                  <input name="nama" type="text" class="form-control">
                </div>
                @error('nama')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label for="exampleTextarea1">Email</label>
                    <input name="email" type="text" class="form-control">
                </div>
                @error('email')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                  <label for="exampleInputCity1">Password</label>
                  <input name="password" type="password" class="form-control">
                </div>
                @error('password')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <button type="submit" class="btn btn-primary mr-2">Submit</button>
                <button class="btn btn-light">Cancel</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection