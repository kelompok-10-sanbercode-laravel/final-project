@extends('layouts.master')

@section('content')
    <div class="content-wrapper">
      <div class="page-header">
        <h3 class="page-title"> Tambah Data Buku </h3>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="">Buku</a></li>
            <li class="breadcrumb-item active" aria-current="page">Tambah Buku</li>
          </ol>
        </nav>
      </div>
      <div class="row">
        <div class="col-12 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
              <form class="forms-sample" action="/buku/{{$buku->id}}" method="POST"  enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="form-group">
                  <label for="exampleInputName1">Pengarang</label>
                  <input name="pengarang" type="text" class="form-control" value="{{$buku->pengarang}}">
                </div>
                @error('pengarang')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label for="exampleTextarea1">Judul</label>
                    <input name="judul" type="text" class="form-control" value="{{$buku->judul}}">
                </div>
                @error('judul')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                  <label for="exampleInputCity1">Penerbit</label>
                  <input name="penerbit" type="text" class="form-control" value="{{$buku->penerbit}}">
                </div>
                @error('penerbit')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                  <label for="exampleInputCity1">Tahun</label>
                  <input name="year" type="text" class="form-control" value="{{$buku->year}}">
                </div>
                @error('year')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                  <label>Kategori Buku</label>
                  <select name="kategori_buku_id" class="form-control" id="">
                    <option value="">--pilih kategori buku--</option>
                    
                    @forelse ($kategori_buku as $item)
                        @if ($item->id === $buku->kategori_buku_id)
                           <option value="{{$item->id}}" selected>{{$item->kategori}}</option>
                            
                        @else
                        <option value="{{$item->id}}">{{$item->kategori}}</option>
                        @endif
                    @empty
                        <option value="">Tidak ada data kategori buku</option>
                    @endforelse
                  </select>
                </div>
                @error('year')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <button type="submit" class="btn btn-primary mr-2">Submit</button>
                <button class="btn btn-light">Cancel</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection