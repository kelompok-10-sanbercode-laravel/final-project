@extends('layouts.master')

@section('content')
    <div class="content-wrapper">
      <div class="page-header">
        <h3 class="page-title"> Tambah Data Anggota </h3>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="">Anggota</a></li>
            <li class="breadcrumb-item active" aria-current="page">Tambah Anggota</li>
          </ol>
        </nav>
      </div>
      <div class="row">
        <div class="col-12 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
              <form class="forms-sample" action="/anggota/{{$anggota->id}}" method="POST"  enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="form-group">
                  <label for="exampleInputName1">Nama</label>
                  <input name="nama" type="text" class="form-control" value="{{$anggota->nama}}">
                </div>
                @error('nama')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                  <label for="exampleTextarea1">Alamat</label>
                  <textarea name="alamat" class="form-control" rows="4">{{$anggota->alamat}}</textarea>
              </div>
              @error('alamat')
              <div class="alert alert-danger">{{ $message }}</div>
              @enderror
                <div class="form-group">
                  <label>Jenis Kelamin</label>
                  <select name="jk" class="form-control" id="">
                    <option value="laki-laki">Laki-laki</option>
                    <option value="perempuan">Perempuan</option>
                  </select>
                </div>
                @error('jk')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                  <label for="exampleInputCity1">No. Hp</label>
                  <input name="hp" type="text" class="form-control" value="{{$anggota->hp}}">
                </div>
                @error('hp')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <button type="submit" class="btn btn-primary mr-2">Submit</button>
                <button class="btn btn-light">Cancel</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection