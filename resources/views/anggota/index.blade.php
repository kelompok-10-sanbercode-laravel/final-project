@extends('layouts.master')

@section('content')
<div class="content-wrapper">
    <div class="page-header">
      <h3 class="page-title"> List Data Anggota </h3>
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Anggota</a></li>
          <li class="breadcrumb-item active" aria-current="page">List Anggota</li>
        </ol>
      </nav>
    </div>
    <div class="row">
      <a href="/anggota/create" class="btn btn-primary mb-3 mx-3">
        Tambah
    </a>
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <div class="table">
              <table class="table">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Alamat</th>
                    <th>Jenis Kelamin</th>
                    <th>No. Hp</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @forelse ($anggota as $key => $value)
                  <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$value->nama}}</td>
                    <td>{{$value->alamat}}</td>
                    <td>{{$value->jk}}</td>
                    <td>{{$value->hp}}</td>
                    <td>
                      <form action="/anggota/{{$value->id}}" method="POST" enctype="multipart/form-data">
                          @csrf
                          @method('DELETE')
                          <a href="/anggota/{{$value->id}}/edit" class="btn btn-warning btn-sm m-1">Edit</a>
                          <input type="submit" value="Delete" class="btn btn-danger btn-sm m-1">
                      </form>
                    </td>
                  </tr>
                  @empty
                  <tr>
                    <td>Tidak Ada Data</td>
                  </tr>
                  @endforelse
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>
@endsection