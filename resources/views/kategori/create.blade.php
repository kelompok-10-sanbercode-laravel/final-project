@extends('layouts.master')

@section('content')
    <div class="content-wrapper">
      <div class="page-header">
        <h3 class="page-title"> Tambah Data Kategori </h3>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="">Kategori</a></li>
            <li class="breadcrumb-item active" aria-current="page">Tambah Kategori</li>
          </ol>
        </nav>
      </div>
      <div class="row">
        <div class="col-12 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
              <form class="forms-sample" action="/kategori_buku" method="POST"  enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                  <label for="exampleInputName1">Kategori</label>
                  <input name="kategori" type="text" class="form-control">
                </div>
                @error('kategori')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <button type="submit" class="btn btn-primary mr-2">Submit</button>
                <a class="btn btn-light" href="/kategori_buku">Cancel</a>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection