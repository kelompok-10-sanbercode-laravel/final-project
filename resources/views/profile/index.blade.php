@extends('layouts.master')

@section('content')
    <div class="content-wrapper">
      <div class="page-header">
        <h3 class="page-title"> Update Profile </h3>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="">Petugas</a></li>
            <li class="breadcrumb-item active" aria-current="page">Tambah Petugas</li>
          </ol>
        </nav>
      </div>
      <div class="row">
        <div class="col-12 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
              <form class="forms-sample" action="/profile/{{$detailProfile->id}}" method="POST" >
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="exampleTextarea1">alamat</label>
                    <textarea name="alamat" class="form-control" rows="4">{{$detailProfile->alamat}}</textarea>
                </div>
                @error('alamat')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                  <label>Jenis Kelamin</label>
                  <select name="jk" class="form-control" id="">
                    <option value="laki-laki">Laki-laki</option>
                    <option value="perempuan">Perempuan</option>
                  </select>
                </div>
                @error('jk')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                  <label for="exampleTextarea1">No. Hp</label>
                  <input name="hp" value="{{$detailProfile->hp}}" type="text" class="form-control">
              </div>
              @error('hp')
              <div class="alert alert-danger">{{ $message }}</div>
              @enderror
                <button type="submit" class="btn btn-primary mr-2">Submit</button>
                <a class="btn btn-light" href="/">Cancel</a>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection