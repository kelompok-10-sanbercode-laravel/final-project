@extends('layouts.master')

@section('content')
    <div class="container-sm position-relative align-self-center flex-column justify-content-center mt-5" data-aos="fade-up" data-aos-delay="100">
      <div class="row justify-content-center">
        <div class="col-xl-7 col-lg-9 text-center">
          <h1 class="h1">Selamat Datang di Libranite</h1>
          <h2 class="h2">Sistem informasi untuk petugas</h2>
        </div>
      </div>
      <div class="container position-relative mt-3 row justify-content-center">
      @guest
        <li class="nav-item dropdown d-none d-lg-block">
          <a class="nav-link btn btn-info create-new-button" id="createbuttonDropdown" aria-expanded="false" href="/login">Get Started</a>
        </li>
     @endguest
      </div>
    </div>
@endsection