<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <div class="sidebar-brand-wrapper d-none d-lg-flex align-items-center justify-content-center fixed-top">
      <a class="sidebar-brand brand-logo" href="/"><img src="{{asset('sbadmin/template/assets/images/Libranite-1.png')}}" alt="logo" /></a>
      <a class="sidebar-brand brand-logo-mini" href="/"><img src="{{asset('sbadmin/template/assets/images/Libranite-1(1).png')}}" alt="logo" /></a>
    </div>
    <ul class="nav">
      
      <li class="nav-item nav-category">
        <span class="nav-link">Navigation</span>
      </li>
      <li class="nav-item menu-items">
        <a class="nav-link" href="/">
          <span class="menu-icon">
            <i class="mdi mdi-speedometer"></i>
          </span>
          <span class="menu-title">Dashboard</span>
        </a>
      </li>
      <li class="nav-item menu-items">
        <a class="nav-link" href="/anggota">
          <span class="menu-icon">
            <i class="mdi mdi-account-box"></i>
          </span>
          <span class="menu-title">Anggota</span>
        </a>
      </li>
      <li class="nav-item menu-items">
        <a class="nav-link" href="/petugas">
          <span class="menu-icon">
            <i class="mdi mdi-account-card-details"></i>
          </span>
          <span class="menu-title">Petugas</span>
        </a>
      </li>
      <li class="nav-item menu-items">
        <a class="nav-link" href="/buku">
          <span class="menu-icon">
            <i class="mdi mdi-library-books"></i>
          </span>
          <span class="menu-title">Buku</span>
        </a>
      </li>
      <li class="nav-item menu-items">
        <a class="nav-link" href="/peminjaman">
          <span class="menu-icon">
            <i class="mdi mdi-chart-bar"></i>
          </span>
          <span class="menu-title">Peminjaman</span>
        </a>
      </li>
      <li class="nav-item menu-items">
        <a class="nav-link" href="/kategori_buku">
          <span class="menu-icon">
            <i class="mdi mdi-book-multiple"></i>
          </span>
          <span class="menu-title">Kategori</span>
        </a>
      </li>
     
    </ul>
  </nav>