<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Register</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{asset('sbadmin/template/assets/vendors/mdi/css/materialdesignicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('sbadmin/template/assets/vendors/css/vendor.bundle.base.css')}}">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="{{asset('sbadmin/template/assets/css/style.css')}}">
    <!-- End layout styles -->
    <link rel="shortcut icon" href="{{asset('sbadmin/template/assets/images/favicon.png')}}"/>
  </head>
  <body>
    <div class="container-scroller">
      <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="row w-100 m-0">
          <div class="content-wrapper full-page-wrapper d-flex align-items-center auth login-bg">
            <div class="card col-lg-4 mx-auto">
              <div class="card-body px-5 py-5">
                <h3 class="card-title text-left mb-3">Register</h3>
                <form method="POST" action="{{ route('register') }}">
                    @csrf
                  <div class="form-group">
                    <label>{{ __('Nama') }}</label>
                    <input type="text" name="nama" class="form-control p_input">
                  </div>
                  @error('nama')
                    <strong>{{ $message }}</strong>
                  @enderror
                  <div class="form-group">
                    <label for="email">{{ __('Email Address') }}</label>
                    <input type="email" name="email" class="form-control p_input @error('email') is-invalid @enderror">
                  </div>
                  @error('email')
                    <strong>{{ $message }}</strong>
                  @enderror
                  <div class="form-group">
                    <label for="password">{{ __('Password') }}</label>
                    <input type="password" name="password" class="form-control p_input @error('password') is-invalid @enderror" >
                  </div>
                  <div class="form-group">
                    <label for="password-confirm">{{ __('Confirm Password') }}</label>
                    <input id="password-confirm" type="password" name="password_confirmation" class="form-control" >
                  </div>
                  @error('password')
                    <strong>{{ $message }}</strong>
                  @enderror
                  <div class="form-group">
                    <label>Alamat</label>
                    <textarea name="alamat" class="form-control" id="exampleTextarea1" rows="4"></textarea>
                  </div>
                  @error('alamat')
                    <strong>{{ $message }}</strong>
                  @enderror
                  <div class="form-group">
                    <label for="exampleSelectGender">Jenis Kelamin</label>
                  <select name="jk" class="form-control" id="exampleSelectGender">
                    <option>Laki-Laki</option>
                    <option>Perempuan</option>
                  </select>
                  </div>
                  @error('jk')
                    <strong>{{ $message }}</strong>
                  @enderror
                  <div class="form-group">
                    <label>Nomor Handphone</label>
                    <input type="text" name="hp" class="form-control p_input">
                  </div>
                  @error('hp')
                    <strong>{{ $message }}</strong>
                  @enderror
                  <div class="form-group d-flex align-items-center justify-content-between">
                    <div class="form-check">
                      {{-- <label class="form-check-label">
                        <input type="checkbox" class="form-check-input"> Remember me </label> --}}
                    </div>
                    {{-- <a href="#" class="forgot-pass">Forgot password</a> --}}
                  </div>
                  <div class="text-center">
                    <button type="submit" class="btn btn-primary btn-block enter-btn">{{ __('Register') }}</button>
                  </div>
                  <p class="sign-up text-center">Sudah punya akun?<a href="/login"> Login</a></p>
                  {{-- <p class="terms">By creating an account you are accepting our<a href="#"> Terms & Conditions</a></p> --}}
                </form>
              </div>
            </div>
          </div>
          <!-- content-wrapper ends -->
        </div>
        <!-- row ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="{{asset('sbadmin/template/assets/vendors/js/vendor.bundle.base.js')}}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="{{asset('sbadmin/template/assets/js/off-canvas.js')}}"></script>
    <script src="{{asset('sbadmin/template/assets/js/hoverable-collapse.js')}}"></script>
    <script src="{{asset('sbadmin/template/assets/js/misc.js')}}"></script>
    <script src="{{asset('sbadmin/template/assets/js/settings.js')}}"></script>
    <script src="{{asset('sbadmin/template/assets/js/todolist.js')}}"></script>
    <!-- endinject -->
  </body>
</html>

{{-- @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('Email Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password" class="col-md-4 col-form-label text-md-end">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-end">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}
