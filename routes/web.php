<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AnggotaController;
use App\Http\Controllers\PetugasController;
use App\Http\Controllers\BukuController;
use App\Http\Controllers\PeminjamanController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfileController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// sudah buat branch

Route::get('/', [HomeController::class, 'index']);

Route::middleware(['auth'])->group(function () {
    
    //Anggota CRUD
    Route::resource('anggota', AnggotaController::class);
    
    //petugas CRUD
    Route::resource('petugas', PetugasController::class);
    
    //CRUD buku
    Route::resource('buku', BukuController::class);
    
    //CRUD Peminjaman
    Route::resource('peminjaman', PeminjamanController::class);

    //CRUD Kategori
    Route::resource('kategori_buku', KategoriController::class);

    Route::resource('profile', ProfileController::class)->only(['index', 'update']);
});

Route::resource('post', PostController::class);

//Auth
Auth::routes();
